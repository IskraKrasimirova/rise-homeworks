﻿using Newtonsoft.Json;

namespace ToDoListWebApp.Models
{
    public class InputEditTaskModel : InputTaskModel
    {
        [JsonProperty(nameof(Id))]
        public int Id { get; set; }

        [JsonProperty(nameof(Done))]
        public bool Done { get; set; }
    }
}
