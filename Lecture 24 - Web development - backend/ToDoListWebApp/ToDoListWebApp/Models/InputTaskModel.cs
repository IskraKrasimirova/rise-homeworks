﻿using Newtonsoft.Json;

namespace ToDoListWebApp.Models
{
    public class InputTaskModel
    {
        [JsonProperty(nameof(Name))]
        public string Name { get; set; }


        [JsonProperty(nameof(CreatedOn))]
        public string CreatedOn { get; set; }

        
        [JsonProperty(nameof(Description))]
        public string Description { get; set; }


        [JsonProperty(nameof(CreatedBy))]
        public string CreatedBy { get; set; }


        [JsonProperty(nameof(AssignedTo))]
        public string AssignedTo { get; set; }
    }
}
