﻿namespace ToDoListWebApp.Models
{
    public class TaskViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime CreatedOn { get; set; }
        public bool Done { get; set; }
        public string Description { get; set; }
        public int AssignmentId { get; set; }
        public virtual AssignmentViewModel Assignment { get; set; }
    }
}
