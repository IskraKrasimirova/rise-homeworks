using NUnit.Framework;
using static P02.EraseMiddleElement.LinkedList;

namespace P02.EraseMiddleElement.Test
{
    [TestFixture]
    public class Tests
    {
        private static LinkedList<int>? list;

        [Test]
        public void EraseMiddleReturnsTrueWithOddListLength()
        {
            list = new LinkedList<int>();
            list.AddFirst(1);
            list.AddLast(2);
            list.AddLast(3);

            Assert.That(EraseMiddle(list), Is.True);
        }

        [Test]
        public void EraseMiddleWithEvenListLengthRemovesElementAfterMiddleAndReturnsTrue()
        {
            list = new LinkedList<int>();
            list.AddFirst(1);
            list.AddLast(2);
            list.AddLast(3);
            list.AddLast(4);

            Assert.That(EraseMiddle(list), Is.True);
        }

        [Test]
        public void EraseMiddleReturnsTrueWhenListHasOnlyOneElement()
        {
            list = new LinkedList<int>();
            list.AddFirst(1);

            Assert.That(EraseMiddle(list), Is.True);
        }

        [Test]
        public void EraseMiddleReturnsTrueWhenListHas2ElementsAndRemovesTheSecondElement()
        {
            list = new LinkedList<int>();
            list.AddFirst(1);
            list.AddLast(2);

            Assert.That(EraseMiddle(list), Is.True);
        }

        [Test]
        public void EraseMiddleReturnsFalseWhenListIsEmpty()
        {
            list = new LinkedList<int>();

            Assert.That(EraseMiddle(list), Is.False);
        }

        [Test]
        public void EraseMiddleThrowsExceptionWhenListIsNull()
        {
            list = null;

            Assert.Throws<NullReferenceException>(() => EraseMiddle(list));
        }
    }
}