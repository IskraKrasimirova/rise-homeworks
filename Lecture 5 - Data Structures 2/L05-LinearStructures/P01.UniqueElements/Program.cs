﻿namespace P01.UniqueElements
{
    public class Program
    {
        static void Main(string[] args)
        {
            List<int> list = new List<int> { 1, 1, 2, 4, 3, 2, 2, 3, 4 };

            Console.WriteLine(string.Join(" ", Unique.GetUnique(list)));
        }
    }

    public class Unique
    {
        public static List<int> GetUnique(List<int> numbers)
        {
            HashSet<int> unique = new HashSet<int>();

            foreach (int number in numbers)
            {
                unique.Add(number);
            }

            return unique.ToList();
        }
    }
}