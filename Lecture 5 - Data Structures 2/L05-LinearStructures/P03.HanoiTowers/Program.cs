﻿using System;

namespace P03.HanoiTowers
{
    public class Program
    {
        static void Main(string[] args)
        {
            Hanoi.Solve(4, 1, 2, 3);
        }
    }

    public class Hanoi
    {
        public static void Solve(int diskCount, int fromPole, int toPole, int viaPole)
        {
            if (diskCount == 1)
            {
                Console.WriteLine("Move disk from pole " + fromPole + " to pole " + toPole);
            }
            else
            {
                Solve(diskCount - 1, fromPole, viaPole, toPole);
                Solve(1, fromPole, toPole, viaPole);
                Solve(diskCount - 1, viaPole, toPole, fromPole);
            }
        }
    }
}