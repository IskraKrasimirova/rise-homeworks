﻿namespace _01.GraphCycleDetector
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Graph graph = new Graph(5);
            //graph.AddEdge(0, 1);
            //graph.AddEdge(0, 2);
            //graph.AddEdge(0, 3);
            //graph.AddEdge(3, 4);

            graph.AddEdge(0, 1);
            graph.AddEdge(0, 2);
            graph.AddEdge(0, 3);
            graph.AddEdge(3, 4);
            graph.AddEdge(1, 2);

            Console.WriteLine(graph.DetectCycle());
        }
    }
}