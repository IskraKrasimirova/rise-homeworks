using NUnit.Framework;

namespace _01.GraphCycleDetector.Test
{
    public class Tests
    {
        Graph graph = new Graph(5);

        [SetUp]
        public void Setup()
        {
            graph.AddEdge(0, 1);
            graph.AddEdge(0, 2);
            graph.AddEdge(0, 3);
            graph.AddEdge(3, 4);
        }

        [Test]
        public void DetectCycleReturnsFalseWhenGraphIsAcyclic()
        {
            Assert.That(graph.DetectCycle(), Is.False);
        }

        [Test]
        public void DetectCycleReturnsTrueWhenGraphIsCyclic()
        {
            graph.AddEdge(1, 2);

            Assert.That(graph.DetectCycle(), Is.True);
        }
    }
}