﻿namespace P02.BinaryTreeHeight
{
    public class BinaryTree
    {
        public BinaryTree(int value, BinaryTree left, BinaryTree right)
        {
            Value = value;
            Left = left;
            Right = right;
        }

        public int Value { get; set; }

        public BinaryTree Left { get; set; }

        public BinaryTree Right { get; set; }

        public static int TreeHeight(BinaryTree node)
        {
            int numberLevels = 0;
            Queue<BinaryTree> nodesToVisit = new Queue<BinaryTree>();

            if (node == null)
            {
                return numberLevels;
            }

            nodesToVisit.Enqueue(node);

            while (nodesToVisit.Count > 0)
            {
                numberLevels++;
                int numberNodesCurrentLevel = nodesToVisit.Count;

                while (numberNodesCurrentLevel > 0)
                {
                    var currentNode = nodesToVisit.Dequeue();

                    if (currentNode.Left != null)
                    {
                        nodesToVisit.Enqueue(currentNode.Left);
                    }

                    if (currentNode.Right != null)
                    {
                        nodesToVisit.Enqueue(currentNode.Right);
                    }

                    numberNodesCurrentLevel--;
                }

            }

            return numberLevels - 1;
        }
    }
}
