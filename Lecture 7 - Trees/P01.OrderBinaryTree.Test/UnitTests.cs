using NUnit.Framework;

namespace P01.OrderBinaryTree.Test
{
    public class Tests
    {
        private BinaryTree binaryTree;
        private List<BinaryTree> result;

        [SetUp]
        public void Setup()
        {
            var node35 = new BinaryTree(35, null, null);
            var node61 = new BinaryTree(61, null, null);
            var node76 = new BinaryTree(76, null, null);

            var node24 = new BinaryTree(24, null, null);
            var node38 = new BinaryTree(38, node35, null);
            var node69 = new BinaryTree(69, node61, node76);

            var node11 = new BinaryTree(11, null, node24);
            var node52 = new BinaryTree(52, node38, node69);

            this.binaryTree = new BinaryTree(25, node11, node52);
            result = new List<BinaryTree>();
        }

        [Test]
        public void DfsPreOrderReturnsCorrectResult()
        {
            var tree = this.binaryTree.DfsPreOrder(binaryTree);
            List<int> expectedValues = new List<int> { 25, 11, 24, 52, 38, 35, 69, 61, 76 };

            Assert.That(tree.Count, Is.EqualTo(expectedValues.Count));
            for (int i = 0; i < expectedValues.Count; i++)
            {
                Assert.That(tree[i].Value, Is.EqualTo(expectedValues[i]));
            }
        }

        [Test]
        public void DfsPostOrderReturnsCorrectResult()
        {
            var tree = this.binaryTree.DfsPostOrder(binaryTree);
            List<int> expectedValues = new List<int> { 24, 11, 35, 38, 61, 76, 69, 52, 25 };

            Assert.That(tree.Count, Is.EqualTo(expectedValues.Count));
            for (int i = 0; i < expectedValues.Count; i++)
            {
                Assert.That(tree[i].Value, Is.EqualTo(expectedValues[i]));
            }
        }

        [Test]
        public void DfsInOrderReturnsCorrectResult()
        {
            var tree = this.binaryTree.DfsInOrder(binaryTree);
            List<int> expectedValues = new List<int> { 11, 24, 25, 35, 38, 52, 61, 69, 76 };

            Assert.That(tree.Count, Is.EqualTo(expectedValues.Count));
            for (int i = 0; i < expectedValues.Count; i++)
            {
                Assert.That(tree[i].Value, Is.EqualTo(expectedValues[i]));
            }
        }
    }
}