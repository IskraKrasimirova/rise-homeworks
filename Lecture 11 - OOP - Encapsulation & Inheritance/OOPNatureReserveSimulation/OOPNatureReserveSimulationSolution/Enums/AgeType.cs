﻿namespace OOPNatureReserveSimulationSolution.Enums
{
    public enum AgeType
    {
        None,
        Young,
        Middle,
        Adult
    }
}
