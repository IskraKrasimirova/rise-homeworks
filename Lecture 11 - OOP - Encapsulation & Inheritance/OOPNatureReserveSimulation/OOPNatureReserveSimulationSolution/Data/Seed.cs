﻿using OOPNatureReserveSimulationSolution.Contracts;
using OOPNatureReserveSimulationSolution.Animals;
using OOPNatureReserveSimulationSolution.Foods;
using OOPNatureReserveSimulationSolution.Map;

namespace OOPNatureReserveSimulationSolution.Data
{
    public class Seed
    {
        private readonly IMap map;

        public Seed()
        {
        }

        public HashSet<IEatable> GenerateFood()
        {
            HashSet<IEatable> foods = new HashSet<IEatable>();

            Food milk = new Food("milk", 4);
            Food freshMeat = new Food("fresh meat", 11);
            Food meat = new Food("meat", 12);
            Food bones = new Food("bones", 10);
            Food flesh = new Food("flesh", 9);
            Food nuts = new Food("nuts", 6);
            Food seeds = new Food("seeds", 4);
            Food fruits = new Food("fruits", 5);
            Food leaves = new Food("leaves", 3);
            Food plant = new Food("plant", 7);
            Food grass = new Food("grass", 2);
            Food bark = new Food("bark", 3);
            Food insects = new Food("insects", 3);
            Food plankton = new Food("plankton", 4);
            Food eggs = new Food("eggs", 10);
            Food honey = new Food("honey", 9);
            Food humanFood = new Food("humanFood", 8);
            Food bigPlants = new Food("big plants", 8);
            IEatable smallAnimals = new Food("small animals", 13);
            IEatable bigAnimals = new Food("big animals", 15);
            IEatable mouse = new Herbivorous("mouse", 3, map);
            IEatable deer = new Herbivorous("deer", 5, map);
            IEatable wolf = new Carnivorous("wolf", 6, map);
            IEatable eagle = new Carnivorous("eagle", 4, map);
            IEatable pig = new Omnivorous("pig", 9, map);

            foods.Add(milk);
            foods.Add(freshMeat);
            foods.Add(meat);
            foods.Add(bones);
            foods.Add(flesh);
            foods.Add(nuts);
            foods.Add(seeds);
            foods.Add(fruits);
            foods.Add(honey);
            foods.Add(humanFood);
            foods.Add(leaves);
            foods.Add(plant);
            foods.Add(grass);
            foods.Add(bark);
            foods.Add(insects);
            foods.Add(plankton);
            foods.Add(eggs);
            foods.Add(bigPlants);
            foods.Add(smallAnimals);
            foods.Add(bigAnimals);
            foods.Add(mouse);
            foods.Add(deer);
            foods.Add(wolf);
            foods.Add(eagle);
            foods.Add(pig);

            return foods;
        }

        public HashSet<Carnivorous> GenerateCarnivora()
        {
            HashSet<Carnivorous> carnivora = new HashSet<Carnivorous>();

            Carnivorous wolf = new Carnivorous("wolf", 6, map);
            Carnivorous tiger = new Carnivorous("tiger", 8, map);
            Carnivorous eagle = new Carnivorous("eagle", 4, map);

            carnivora.Add(wolf);
            carnivora.Add(tiger);
            carnivora.Add(eagle);

            return carnivora;
        }

        public HashSet<Herbivorous> GenerateHerbivorous()
        {
            HashSet<Herbivorous> herbivorous = new HashSet<Herbivorous>();

            Herbivorous deer = new Herbivorous("deer", 5, map);
            Herbivorous mouse = new Herbivorous("mouse", 3, map);

            herbivorous.Add(deer);
            herbivorous.Add(mouse);

            return herbivorous;
        }

        public HashSet<Omnivorous> GenerateOmnivora()
        {
            HashSet<Omnivorous> omnivora = new HashSet<Omnivorous>();

            Omnivorous bear = new Omnivorous("bear", 10, map);
            Omnivorous pig = new Omnivorous("pig", 9, map);

            omnivora.Add(bear);
            omnivora.Add(pig);

            return omnivora;
        }
    }
}
