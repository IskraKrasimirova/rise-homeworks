﻿using OOPNatureReserveSimulationSolution.Animals;
using OOPNatureReserveSimulationSolution.Contracts;
using OOPNatureReserveSimulationSolution.Enums;
using OOPNatureReserveSimulationSolution.Foods;
using OOPNatureReserveSimulationSolution.Map;

namespace OOPNatureReserveSimulationSolution.Bioms
{
    public class Savana : Biome
    {
        public Savana(int capacity, IMap map) 
            : base(capacity, map)
        {
            this.Animals = new List<Animal>()
            {
                new Herbivorous("deer", 5, map),
                new Herbivorous("deer", 6, map),
                new Herbivorous("deer", 7, map),
                new Carnivorous("wolf", 6, map),
                new Carnivorous("wolf", 7, map),
                new Carnivorous("wolf", 8, map),
                new Carnivorous("tiger", 8, map),
                new Carnivorous("tiger", 9, map)
            };

            this.Foods = new List<IEatable>()
            {
                new Food("grass", 2),
                new Food("leaves",3),
                new Food("seeds",4),
                new Food("fruits",5),
                new Food("plant",7),
                new Food("nuts",6),
                new Food("big plants",8),
                new Food("bark",3),
                new Food("milk",4),
                new Food("insects",3),
                new Food("fresh meat",11),
                new Food("meat",12),
                new Food("bones",10),
                new Food("small animals",15),
                new Food("big animals",15),
                new Food("wolf", 20),
                new Food("deer", 15)
            };
        }

        public override BiomType Type { get; init; } = BiomType.Savana;
    }
}
