﻿using OOPNatureReserveSimulationSolution.Animals;
using OOPNatureReserveSimulationSolution.Contracts;
using OOPNatureReserveSimulationSolution.Enums;
using OOPNatureReserveSimulationSolution.Foods;
using OOPNatureReserveSimulationSolution.Map;

namespace OOPNatureReserveSimulationSolution.Bioms
{
    public class Plain : Biome
    {
        public Plain(int capacity, IMap map) 
            : base(capacity, map)
        {
            this.Animals = new List<Animal>()
            {
                new Herbivorous("mouse", 3, map),
                new Herbivorous("mouse", 4, map),
                new Herbivorous("mouse", 5, map),
                new Herbivorous("mouse", 6, map),
                new Herbivorous("mouse", 2, map),
                new Carnivorous("wolf", 6, map),
                new Carnivorous("wolf", 7, map),
                new Carnivorous("wolf", 8, map),
                new Carnivorous("eagle", 4, map),
                new Carnivorous("eagle", 5, map),
                new Omnivorous("pig", 9, map),
                new Omnivorous("pig", 10, map)
            };

            this.Foods = new List<IEatable>()
            {
                new Food("grass", 2),
                new Food("leaves",3),
                new Food("seeds",4),
                new Food("fruits",5),
                new Food("plant",7),
                new Food("nuts",6),
                new Food("big plants",8),
                new Food("bark",3),
                new Food("milk",4),
                new Food("insects",3),
                new Food("fresh meat",11),
                new Food("meat",12),
                new Food("bones",10),
                new Food("small animals",15),
                new Food("big animals",15),
                new Food("mouse", 15),
                new Food("wolf", 20),
                new Food("eagle", 20),
                new Food("pig", 25),
                new Food("honey",9),
                new Food("eggs",10),
                new Food("human food",8)
            };
        }

        public override BiomType Type { get; init; } = BiomType.Plain;
    }
}
