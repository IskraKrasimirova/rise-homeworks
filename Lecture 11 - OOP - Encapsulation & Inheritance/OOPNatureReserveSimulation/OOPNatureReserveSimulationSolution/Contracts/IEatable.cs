﻿namespace OOPNatureReserveSimulationSolution.Contracts
{
    public interface IEatable
    {
        string Name { get; }
        int Nutrition { get; set; }
        int MaxNutrition { get; init; }
        bool IsAnimalFood();
        int ReduceNutrition(int energyDifference);
    }
}
