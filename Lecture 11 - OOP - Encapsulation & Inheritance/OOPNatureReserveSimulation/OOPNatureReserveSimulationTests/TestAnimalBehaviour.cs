﻿using OOPNatureReserveSimulationSolution.Animals;
using OOPNatureReserveSimulationSolution.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOPNatureReserveSimulationTests
{
    public class TestAnimalBehaviour : IBehaviour
    {
        public bool IsHungry = false;

        public string CompletelyEat(Animal animal, IEatable food)
        {
            throw new NotImplementedException();
        }

        public string Die(Animal animal)
        {
            throw new NotImplementedException();
        }

        public string Hungry(Animal animal)
        {
            throw new NotImplementedException();
        }

        public string NotEat(Animal animal, IEatable food)
        {
            throw new NotImplementedException();
        }

        public string NotHungry(Animal animal)
        {
            throw new NotImplementedException();
        }

        public string PartiallyEat(Animal animal, IEatable food)
        {
            throw new NotImplementedException();
        }

        public string Starve(Animal animal)
        {
            throw new NotImplementedException();
        }
    }
}
