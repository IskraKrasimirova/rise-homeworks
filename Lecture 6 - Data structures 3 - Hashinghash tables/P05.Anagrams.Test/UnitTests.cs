using NUnit.Framework;

namespace P05.Anagrams.Test
{
    public class Tests
    {
        [Test]
        public void AreAnagramReturnsTrueWhenInputWordsAreAnagrams()
        {
            string first = "swords";
            string second = "dswosr";

            Assert.That(Anagram.AreAnagram(first, second), Is.True);
        }

        [Test]
        public void AreAnagramReturnsFalseWhenInputWordsAreNotAnagrams()
        {
            string first = "swords";
            string second = "dswost";

            Assert.That(Anagram.AreAnagram(first, second), Is.False);
        }

        [Test]
        public void AreAnagramReturnsFalseWhenInputWordsHaveDifferentLength()
        {
            string first = "swords";
            string second = "dswor";

            Assert.That(Anagram.AreAnagram(first, second), Is.False);
        }
    }
}