﻿using System.Security.Cryptography;
using System.Text;

namespace P06.PasswordHash
{
    public class User
    {
        public string Username { get; set; }
        public string Password { get; set; }
    }
}
