﻿namespace P06.PasswordHash
{
    public class Program
    {
        private static UserService userService;
        static void Main(string[] args)
        {
            userService = new UserService();

            var user1 = new User
            {
                Username = "Iskra",
                Password = "123456"
            };

            var user2 = new User
            {
                Username = "Pesho",
                Password = "Gosho!123"
            };

            var user3 = new User
            {
                Username = "Gosho",
                Password = "123Pesh0*"
            };

            UserService.RegisterUser(user1);
            UserService.RegisterUser(user2);
            UserService.RegisterUser(user3);
            //UserService.RegisterUser(user3);


            Console.WriteLine(UserService.IsLoginCorrect(user1));
            Console.WriteLine(UserService.IsLoginCorrect(user2));
            Console.WriteLine(UserService.IsLoginCorrect(user3));
        }
    }
}