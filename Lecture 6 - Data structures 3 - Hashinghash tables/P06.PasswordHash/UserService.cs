﻿using System.Security.Cryptography;
using System.Text;

namespace P06.PasswordHash
{
    public class UserService
    {
        private static ICollection<User> users;

        public UserService()
        {
            users = new HashSet<User>();
        }

        public static void RegisterUser(User model)
        {
            var userExists = GetUserByUsername(model.Username) != null;

            if (userExists)
            {
                throw new ArgumentException("Registration failed");
            }

            User user = new User()
            {
                Username = model.Username
            };

            user.Password = HashPassword(model.Password);
            users.Add(user);
        }

        public static bool IsLoginCorrect(User model)
        {
            bool isCorrect = false;

            var user = GetUserByUsername(model.Username);

            if (user != null)
            {
                isCorrect = user.Password == HashPassword(model.Password);
            }

            return isCorrect;
        }

        private static User GetUserByUsername(string username)
        {
            return users.FirstOrDefault(u => u.Username == username);
        }

        private static string HashPassword(string password)
        {
            byte[] passwordArray = Encoding.UTF8.GetBytes(password);

            using (SHA256 sha256 = SHA256.Create())
            {
                return Convert.ToBase64String(sha256.ComputeHash(passwordArray));
            }
        }
    }
}
