﻿namespace P04.SpellChecker
{
    public class Checker
    {
        public static string[] Check(HashSet<string> wordsList, string input)
        {
            HashSet<string> inputWords = input.ToLower().Split(" ", StringSplitOptions.RemoveEmptyEntries).ToHashSet();

            string[] misspellWords = inputWords.Except(wordsList).ToArray();

            return misspellWords;
        }
    }
}
